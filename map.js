function mapObject(obj, cb){
    const result = {};

    for(let key in obj){
        if(obj[key] !== undefined){
            result[key]= cb(obj[key]);
        }
    }

    return result;
}

module.exports = mapObject;