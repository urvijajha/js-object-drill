function values(obj){
    const output = [];
    for(let key in obj){
        output.push(obj[key]);
    }

    return output;
}

module.exports = values;